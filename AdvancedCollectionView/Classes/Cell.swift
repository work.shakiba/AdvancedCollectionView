//
//  Cell.swift
//  Pods
//
//  Created by mohsen shakiba on 4/13/1396 AP.
//
//

open class Cell: UICollectionViewCell {
    
    weak var row: RowProtocol?
    public var borderEdge: BorderEdge  = .bottom
    
    open override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func notifySizeChanged() {
        row?.section?.notifySizeChange()
    }
    
    public func emit(event: String, value: Any?) {
        row?.emit(event: event, value: value)
    }
    
    open func highlighted() {
    }
    
    open func unHighlighted() {
    }
    
    open func selected() {
    }
    
    open func deSelected() {
    }
    
    open func borderInsets() -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    open func borderColor() -> UIColor {
        return UIColor(white: 0.95, alpha: 1)
    }
    
    func updateBorder(for index: Int) {
        if self.borderEdge == .all {
            if index == 0 {
                self.contentView.borderIfNeeded(.top, color: borderColor(), inset: borderInsets())
            }
            self.contentView.borderIfNeeded(.bottom, color: borderColor(), inset: borderInsets())
        }else if borderEdge == .bottom {
            self.contentView.borderIfNeeded(.bottom, color: borderColor(), inset: borderInsets())
        }
    }
    
}


public protocol SkeletonCell {
    
    static func staticHeight() -> CGFloat
    
}


