//
//  ListViewController.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import Foundation
import UIKit

/**
 * the class used by view controllers to implement the list view
 **/

open class AdvancedCollectionViewwController: UIViewController, AdvancedTableViewControllerProtocol, RowManager {
    
    var repo = SectionRepo()
    
    /// return current instance of tableView
    public var collectionView: UICollectionView!
    
    /// constraints used for table view
    public let tableViewConstraints: ListViewConstraints = ListViewConstraints()
    
    override open func viewDidLoad() {
        let folowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: folowLayout)
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        super.viewDidLoad()
        self.setupCollectionView()
        collectionView.performBatchUpdates({ 
            self.setup()
        }) { (_) in
            
        }
    }
    
    open func setup() {
        
    }
    
    public func accept(row: RowProtocol) {
        let s = StaticSection() { s in
        }
        row.append(to: s)
        row.section = s
        let index: Int = row.index == -1 ? repo.count : row.index
        _ = s.at(index)
        _ = s.finalize(self)
    }
    
    public func row(by id: String) -> RowProtocol? {
        for section in repo.sections where section is StaticSection {
            let staticSection = section as! StaticSection
            if let row = staticSection.row(by: id) {
                return row
            }
        }
        return nil
    }
    
    internal func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.keyboardDismissMode = .onDrag
        view.addSubview(collectionView)
        addConstraints()
    }
    
    internal func addConstraints() {
        tableViewConstraints.top = NSLayoutConstraint(item: collectionView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: topLayoutGuide, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        tableViewConstraints.bottom = NSLayoutConstraint(item: collectionView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: bottomLayoutGuide, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        tableViewConstraints.left = NSLayoutConstraint(item: collectionView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        tableViewConstraints.right = NSLayoutConstraint(item: collectionView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        view.addConstraints([tableViewConstraints.top, tableViewConstraints.bottom, tableViewConstraints.left, tableViewConstraints.right])
    }
    
    func accept(section: SectionRowRepositoryPrototcol) {
        let index: Int = section.index == -1 ? repo.count : section.index
        repo.insert(section: section, at: index)
        let indexSet = NSIndexSet(index: index) as IndexSet
        collectionView.insertSections(indexSet)
    }
    
}

extension AdvancedCollectionViewwController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = repo.section(by: section)
        return section.cellCount
    }
    
    public func numberOfSections(in tableView: UICollectionView) -> Int {
        let count = repo.count
        return count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = repo.section(by: indexPath.section)
        let cell = section.cell(for: indexPath.row)
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let section = repo.section(by: indexPath.section)
        let height = section.rowHeight(for: indexPath.row)
        return CGSize(width: view.bounds.width, height: height)
    }
    
}

extension AdvancedCollectionViewwController: UICollectionViewDelegate {
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = repo.section(by: indexPath.section)
        section.cell(event: RowEvent.selected, for: indexPath.row)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let section = repo.section(by: indexPath.section)
        section.cell(event: RowEvent.deSelected, for: indexPath.row)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let section = repo.section(by: indexPath.section)
        section.cell(event: RowEvent.highlight, for: indexPath.row)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let section = repo.section(by: indexPath.section)
        section.cell(event: RowEvent.unHighlight, for: indexPath.row)
    }
    
}


public class ListViewConstraints {
    public var top: NSLayoutConstraint!
    public var bottom: NSLayoutConstraint!
    public var left: NSLayoutConstraint!
    public var right: NSLayoutConstraint!
}

class SectionRepo: SectionRepoProtocol {
    
    var sections: [SectionRowRepositoryPrototcol] = []
    
}

protocol SectionRepoProtocol: class {
    
    var sections: [SectionRowRepositoryPrototcol] { get set }
    var count: Int { get }
    
    func section(by index: Int) -> SectionRowRepositoryPrototcol
    func section(by id: String) -> SectionProtocol?
    func insert(section: SectionRowRepositoryPrototcol, at: Int?)
    func remove(section at: Int)
    
}

extension SectionRepoProtocol {
    
    var count: Int {
        return sections.count
    }
    
    func section(by id: String) -> SectionProtocol? {
        return sections.first(where: {$0.id == id}) as? SectionProtocol
    }
    
    func insert(section: SectionRowRepositoryPrototcol, at: Int?) {
        if let at = at {
            sections.insert(section, at: at)
        }else{
            sections.append(section)
        }
        updateIndex()
    }
    
    func section(by index: Int) -> SectionRowRepositoryPrototcol {
        return sections[index]
    }
    
    func remove(section at: Int) {
        sections.remove(at: at)
        updateIndex()
    }
    
    func updateIndex() {
        for (i, section) in sections.enumerated() {
            section.index = i
        }
    }
    
}

protocol AdvancedTableViewControllerProtocol: class {
    
    var collectionView: UICollectionView! { get }
    
}


public protocol RowManager {
    
    func accept(row: RowProtocol)
    
}

