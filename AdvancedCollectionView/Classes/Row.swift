//
//  Row.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

// reload with closure
// reload
//

// better dynamic section delegate naming
// view for empty status

import UIKit


open class Row<T>: BaseRow, PrivateRowProtocol where T: UICollectionViewCell {
    
    public override init() {}
    
    open func setup(cell: T) {}
    
    open func row(selected cell: T) {}
    open func row(deselected cell: T) {}
    open func row(highlighted cell: T) {}
    open func row(unhighlighted cell: T) {}
    open func row(disappeared cell: T) {}
    
    public override func append(to: RowManager) {
        setupID()
        setupNib()
        to.accept(row: self)
    }
    
    func setupID() {
        if id.isEmpty {
            id = String.randomString(length: 10)
        }
    }
    
    func setupNib() {
        let bundle = Bundle(for: T.classForCoder())
        let nib = UINib.fromType(type: T.self, bundle: bundle)
        let nibName = UINib.name(type: T.self)
        self.reuseIdentifier = nibName
        self.nib = nib
    }
    
    override func rowSelected() {
        super.rowSelected()
        self.row(selected: innerCell as! T)
    }
    
    override func rowDeselected() {
        self.row(deselected: innerCell as! T)
    }
    
    override func rowHighlighted() {
        self.row(highlighted: innerCell as! T)
    }
    
    override func rowUnhighlighted() {
        self.row(unhighlighted: innerCell as! T)
    }
    
    override func rowDisappeared() {
        self.row(disappeared: innerCell as! T)
    }
    
    override func setup() {
        self.setup(cell: innerCell as! T)
    }
    
}

open class BaseRow : NSObject, RowProtocol {
    
    public func append(to: RowManager) {
        fatalError("cannot call appen from here")
    }
    
    public var id: String = ""
    public var reuseIdentifier: String = ""
    public var nib: UINib!
    public var index: Int = -1
    public var innerCell: UICollectionViewCell!
    public weak var section: StaticSection?
    public var rowActions: [UITableViewRowAction] = []
    public var borderEdge: BorderEdge? {
        didSet {applyBorderEdge()}
    }
    
    public var clickHandler: (() -> Void)?
    public var updateHandler: (() -> Void)?
    public var eventHandler: ((String, Any?) -> Void)?
    var editingActions: [UITableViewRowAction] = []
    
    open func height() -> CGFloat {
        fatalError("height not overriden")
    }
    
    override init() {
    }
    
    open func cell(disappeared cell: UICollectionViewCell) {}
    open func cell(appeared cell: UICollectionViewCell) {}
    
    public func updateTableViewDimentions() {
        UIView.setAnimationsEnabled(false)
        section?.notifySizeChange()
        UIView.setAnimationsEnabled(true)
    }
    
    public func remove() {
        assert(section != nil)
        assert(index != -1)
        section?.remove(row: self.index)
    }
    
    public func reload() {
        assert(section != nil)
        assert(index != -1)
        section?.reload(row: index)
    }
    
    public func emit(event: String, value: Any?) {
        if let handler = self.eventHandler {
            handler(event, value)
        }
    }
    
    public func commit(_ section: StaticSection) {
        fatalError("commit not implemented")
    }
    
    public func id(_ id: String) -> Self {
        self.id = id
        return self
    }
    
    public func at(_ index: Int) -> Self {
        self.index = index
        return self
    }
    
    public func event(_ handler: @escaping (String, Any?) -> Void) -> Self {
        self.eventHandler = handler
        return self
    }
    
    public func click(_ handler: @escaping () -> Void) -> Self {
        clickHandler = handler
        return self
    }
    
    public func click<T>(handler: @escaping (T?) -> Void) -> Self where T: AdvancedCollectionViewwController {
        let handler = {
            let controller = self.section?.controller
            handler(controller as? T)
        }
        self.clickHandler = handler
        return self
    }
    
    open func on(event: RowEvent) {
        switch event {
        case .highlight:
            self.rowHighlighted()
        case .unHighlight:
            self.rowUnhighlighted()
        case .selected:
            self.rowSelected()
        case .deSelected:
            self.rowDeselected()
        case .willDisplay:
            self.cellWillDisplay()
        case .willDisppear:
            self.rowDisappeared()
            
        }
    }
    
    func cellWillDisplay() {
        updateHandler?()
        setup()
        applyBorderEdge()
    }
    
    func applyBorderEdge() {
        if let cell = innerCell as? Cell {
            if let borderEdge = self.borderEdge {
                cell.borderEdge = borderEdge
            }
            cell.updateBorder(for: self.index)
        }
    }
    
    public func editingActions(_ handler: () -> [UITableViewRowAction]) -> Self {
        rowActions = handler()
        return self
    }
    
    func setup() {}
    func rowHighlighted() {}
    func rowDisappeared() {}
    func rowUnhighlighted() {}
    func rowDeselected() {}
    
    func rowSelected() {
        if let handler = self.clickHandler {
            handler()
        }
    }
}


protocol PrivateRowProtocol: class {
    
    func setupNib()
    func setupID()
    func setup()
}

public protocol RowProtocol: class {
    
    var id: String { get }
    var borderEdge: BorderEdge? { get set }
    var rowActions: [UITableViewRowAction] { get set }
    var innerCell: UICollectionViewCell! { get set }
    var index: Int { get set }
    var reuseIdentifier: String { get set }
    var nib: UINib! { get set }
    var section: StaticSection? { get set }
    var clickHandler: (() -> Void)? { get set }
    var updateHandler: (() -> Void)? { get set }
    var eventHandler: ((String, Any?) -> Void)? { get set }
    
    func append(to: RowManager)
    func height() -> CGFloat
    func on(event: RowEvent)
    func emit(event: String, value: Any?)
    func click(_ handler: @escaping () -> Void) -> Self
    func event(_ handler: @escaping (String, Any?) -> Void) -> Self
    func editingActions(_ handler: () -> [UITableViewRowAction]) -> Self
    func at(_ index: Int) -> Self
    func id(_ id: String) -> Self
    
    func remove()
    func reload()
    
}

extension RowProtocol {
    
    public func customize(_ handler: (Self) -> Void) -> Self {
        handler(self)
        return self
    }
    
    public func click<T>(_ handler: @escaping (T) -> () -> () ) -> Self {
        let action = { [weak self] in
            handler(self?.section?.controller as! T)()
        }
        clickHandler = action
        return self
    }
    
    public func update<T>(_ handler: @escaping (T) -> (Self) -> () ) -> Self where T: AdvancedCollectionViewwController {
        let action = { [weak self] in
            handler(self?.section?.controller as! T)(self!)
        }
        updateHandler = action
        return self
    }
    
    public func event<T>(_ handler: @escaping (T) -> (String, Any?) -> () ) -> Self {
        let action = { [weak self] (event, value)in
            handler(self?.section?.controller as! T)(event, value)
        }
        eventHandler = action
        return self
    }
    
}


public enum RowEvent {
    
    case highlight
    case unHighlight
    case selected
    case deSelected
    case willDisplay
    case willDisppear
    
}
