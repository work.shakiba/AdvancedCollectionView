//
//  Section.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

import UIKit

protocol SectionRowRepositoryPrototcol: class {
    
    var id: String { get }
    var index: Int { get set }
    weak var controller: AdvancedTableViewControllerProtocol? { get set }
    var cellCount: Int { get }
    
    func cell(for row: Int) -> UICollectionViewCell
    func rowHeight(for row: Int) -> CGFloat
    func cell(event: RowEvent, for row: Int)
    
}

public protocol SectionProtocol: class {
    
    var index: Int { get set }
    var id: String { get }
    
    var cellCount: Int { get }
    
    func at(_ at: Int) -> Self
    func id(_ id: String) -> Self
    func finalize(_ controller: AdvancedCollectionViewwController) -> Self
}

public class StaticSection: SectionRowRepositoryPrototcol, SectionProtocol, RowManager {
    
    public var index: Int = -1
    weak var controller: AdvancedTableViewControllerProtocol?
    var rows: [RowProtocol] = []
    public var id: String = ""
    
    public init(_ controller: AdvancedCollectionViewwController? = nil ,_ handler: (StaticSection) -> Void) {
        self.controller = controller
        handler(self)
    }
    
    public var cellCount: Int {
        return rows.count
    }
    
    func cell(for row: Int) -> UICollectionViewCell {
        let indexPath = IndexPath(item: row, section: index)
        let row = rows[row]
        let identifier = row.reuseIdentifier
        let nib = row.nib
        controller?.collectionView.register(nib, forCellWithReuseIdentifier: identifier)
        let cell = controller!.collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        row.innerCell = cell
        row.on(event: .willDisplay)
        if let cell = cell as? Cell {
            cell.row = row
        }
        return cell
    }
    
    func rowHeight(for row: Int) -> CGFloat {
        let row = rows[row]
        return row.height()
    }
    
    public func finalize(_ controller: AdvancedCollectionViewwController) -> Self {
        self.controller = controller
        controller.accept(section: self)
        reIndex()
        return self
    }
    
    public func id(_ id: String) -> Self {
        self.id = id
        return self
    }
    
    public func at(_ at: Int) -> Self {
        index = at
        return self
    }
    
    public func row(by id: String) -> RowProtocol? {
        return rows.first(where: {$0.id == id})
    }
    
    public func row(at index: Int) -> RowProtocol {
        return rows[index]
    }
    
    func canEdit(for row: Int) -> Bool {
        let row = rows[row]
        return row.rowActions.count > 0
    }
    
    func cell(event: RowEvent, for row: Int) {
        let row = rows[row]
        row.on(event: event)
    }
    
    public func accept(row: RowProtocol) {
        rows.append(row)
        row.section = self
        let rowIndex = row.index == -1 ? self.rows.count - 1 : row.index
        let indexPath = IndexPath(item: rowIndex, section: index)
        reIndex()
        controller?.collectionView.insertItems(at: [indexPath])
    }
    
    func reload(row at: Int) {
        let indexPath = IndexPath(item: at, section: index)
        controller?.collectionView.reloadItems(at: [indexPath])
    }
    
    func remove(row at: Int) {
        let indexPath = IndexPath(item: at, section: index)
        rows.remove(at: at)
        reIndex()
        controller?.collectionView.deleteItems(at: [indexPath])
    }
    
    public func clear() {
        self.rows.removeAll()
        let indexSet = NSIndexSet(index: index) as IndexSet
        self.controller?.collectionView.reloadSections(indexSet)
    }
    
    func notifySizeChange() {

    }
    
    func reIndex() {
        if controller == nil { return }
        for (i, row) in rows.enumerated() {
            row.index = i
        }
    }

}

enum EmptyRowStatus {
    case watingToBeAdded
    case added
}

public class DynamicSection: SectionProtocol, SectionRowRepositoryPrototcol {
    
    
    weak var controller: AdvancedTableViewControllerProtocol?
    
    public var index: Int = -1
    public var id: String
    public var cellCount: Int {
        if loading {
            return 20
        }
        if emptyRowMode {
            return 1
        }
        return delegate!.dynamicSection(cellCount: self)
    }
    public var loading = false
    public var emptyRow: Bool = false
    var emptyRowMode = false
    
    weak var delegate: DynamicSectionDelegate?
    
    public init(_ delegate: DynamicSectionDelegate) {
        self.delegate = delegate
        self.id = String.randomString(length: 20)
    }
    
    public func finalize(_ controller: AdvancedCollectionViewwController) -> Self {
        self.controller = controller
        controller.accept(section: self)
        return self
    }
    
    public func id(_ id: String) -> Self {
        self.id = id
        return self
    }
    
    public func at(_ at: Int) -> Self {
        self.index = at
        return self
    }
    
    func cell(for row: Int) -> UICollectionViewCell {
        let repo = CellRepository(collectionView: controller!.collectionView, section: index, id: self.id, row: row)
        if loading {
            let cell = delegate?.dynamicSection(skeletonCell: self, repo: repo)
            return cell as! UICollectionViewCell
        }
        if emptyRowMode {
            let cell = delegate?.dynamicSection(emptyCell: self, repo: repo)
            return cell!
        }
        let cell = delegate!.dynamicSection(cell: self, row: row, repo: repo)
        if let cell = cell as? Cell {
            cell.updateBorder(for: row)
        }
        return cell
    }
    
    func cell(event: RowEvent, for row: Int) {
        let repo = CellRepository(collectionView: controller!.collectionView, section: index, id: self.id, row: row)
        self.delegate?.dynamicSection(event: self, event: event, row: row, repo: repo)
        if event == .selected {
            delegate?.dynamicSection(selected: self, row: row, repo: repo)
        }
        guard let cell = repo.cell(for: row) as? Cell else { return }
        switch event {
        case .willDisplay:
            cell.updateBorder(for: row)
            break
        case .highlight:
            cell.highlighted()
        case .unHighlight:
            cell.unHighlighted()
        case .selected:
            cell.selected()
        case .deSelected:
            cell.deSelected()
        case.willDisppear:
            break
        }
    }
    
    func rowHeight(for row: Int) -> CGFloat {
        if loading {
            return delegate!.dynamicSection(heightForSkeletonCell: self)
        }
        if emptyRowMode {
            return delegate!.dynamicSection(heightForEmptyCell: self)
        }
        if let height = delegate?.dynamicSection(height: self, row: row) {
            return height
        }
        fatalError("height missing")
    }
    
    public func batch(inserts: [Int], updates: [Int], deletes: [Int]) {
        let inserts = inserts.map { (row) -> IndexPath in
            return IndexPath(item: row, section: self.index)
        }
        let updates = updates.map { (row) -> IndexPath in
            return IndexPath(item: row, section: self.index)
        }
        let deletes = deletes.map { (row) -> IndexPath in
            return IndexPath(item: row, section: self.index)
        }
        controller?.collectionView.performBatchUpdates({
            if self.loading {
                let sekeletonRows = (0...19).map({ (row) -> IndexPath in
                    return IndexPath(item: row, section: self.index)
                })
                self.controller?.collectionView.deleteItems(at: sekeletonRows)
                self.loading = false
            }
            if self.emptyRowMode && self.delegate!.dynamicSection(cellCount: self) > 0 {
                self.emptyRowMode = false
                let indexPath = IndexPath(item: 0, section: self.index)
                self.controller?.collectionView.deleteItems(at: [indexPath])
            }
            self.controller?.collectionView.insertItems(at: inserts)
            self.controller?.collectionView.reloadItems(at: updates)
            self.controller?.collectionView.deleteItems(at: deletes)
            if self.delegate!.dynamicSection(cellCount: self) == 0 && self.emptyRow {
                self.emptyRowMode = true
                let indexPath = IndexPath(item: 0, section: self.index)
                self.controller?.collectionView.insertItems(at: [indexPath])
            }
        }, completion: { (_) in
            
        })
    }
    
    public func notifySizeChange() {
        UIView.setAnimationsEnabled(false)
        UIView.setAnimationsEnabled(true)
    }
    
    public func reload() {
        let indexSet = NSIndexSet(index: index) as IndexSet
        if delegate!.dynamicSection(cellCount: self) > 0 {
            loading = false
        }
        if delegate!.dynamicSection(cellCount: self) > 0 {
            emptyRowMode = false
        }
        self.controller?.collectionView.reloadSections(indexSet)
    }
    
}

public protocol DynamicSectionDelegate: class {
    
    func dynamicSection(cellCount section: DynamicSection) -> Int
    func dynamicSection(cell section: DynamicSection, row: Int, repo: CellRepository) -> UICollectionViewCell
    func dynamicSection(height section: DynamicSection, row: Int) -> CGFloat
    
    func dynamicSection(event section: DynamicSection, event: RowEvent, row: Int, repo: CellRepository)
    func dynamicSection(selected section: DynamicSection, row: Int, repo: CellRepository)
    func dynamicSection(editingActions section: DynamicSection, row: Int) -> [UITableViewRowAction]
    func dynamicSection(skeletonCell section: DynamicSection, repo: CellRepository) -> SkeletonCell
    func dynamicSection(heightForSkeletonCell section: DynamicSection) -> CGFloat
    func dynamicSection(emptyCell section: DynamicSection, repo: CellRepository) -> UICollectionViewCell
    func dynamicSection(heightForEmptyCell section: DynamicSection) -> CGFloat
    
}

extension DynamicSectionDelegate {
    
    public func dynamicSection(event section: DynamicSection, event: RowEvent, row: Int, repo: CellRepository) {
    }
    
    public func dynamicSection(selected section: DynamicSection, row: Int, repo: CellRepository) {
    }
    
    public func dynamicSection(editingActions section: DynamicSection, row: Int) -> [UITableViewRowAction]{
        return []
    }
    
    func dynamicSection(skeletonCell section: DynamicSection, repo: CellRepository) -> SkeletonCell {
        fatalError("SkeletonCell missing")
    }
    
    func dynamicSection(heightForSkeletonCell section: DynamicSection) -> CGFloat {
        return 50
    }
    
}


public class CellRepository {
    
    var collectionView: UICollectionView
    var section: Int
    var id: String
    var row: Int
    
    var registerdCells: [String: Bool] = [:]
    
    init(collectionView: UICollectionView, section: Int, id: String, row: Int) {
        self.row = row
        self.section = section
        self.collectionView = collectionView
        self.id = id
    }
    
    public func dequeue<T>(for type: T.Type) -> T where T: UICollectionViewCell {
        let indexPath = IndexPath(item: row, section: section)
        let bundle = Bundle(for: T.classForCoder())
        let nib = UINib.fromType(type: T.self, bundle: bundle)
        let nibName = UINib.name(type: T.self) + id
        collectionView.register(nib, forCellWithReuseIdentifier: nibName)
        return collectionView.dequeueReusableCell(withReuseIdentifier: nibName, for: indexPath) as! T
    }
    
    public func cell<T>(for row: Int) -> T? where T: UICollectionViewCell {
        return collectionView.visibleCells[row] as? T
    }
    
}
