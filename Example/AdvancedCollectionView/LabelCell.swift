//
//  LabelCell.swift
//  AdvancedCollectionView
//
//  Created by mohsen shakiba on 5/18/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedCollectionView

class LabelCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

class LabelRow: Row<LabelCell> {
    
    let text: String
    
    init(text: String) {
        self.text = text
    }
    
    override func setup(cell: LabelCell) {
        cell.label.text = text
    }
    
    override func height() -> CGFloat {
        return 50
    }
    
}
