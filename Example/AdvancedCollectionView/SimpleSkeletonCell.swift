//
//  SimpleSkeletonCell.swift
//  AdvancedCollectionView
//
//  Created by mohsen shakiba on 5/18/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedCollectionView

class SimpleSkeletonCell: UICollectionViewCell, SkeletonCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func staticHeight() -> CGFloat {
        return 60
    }

}
