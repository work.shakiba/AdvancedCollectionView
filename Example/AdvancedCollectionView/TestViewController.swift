//
//  TestViewController.swift
//  AdvancedCollectionView
//
//  Created by mohsen shakiba on 5/18/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedCollectionView

class TestViewController: AdvancedCollectionViewwController {
    
    var itemCount = 0
    var section: DynamicSection!
    
    override func setup() {
        
        LabelRow(text: "click to add")
        .click(TestViewController.addRow)
        .append(to: self)
        
        LabelRow(text: "click to add many")
            .click(TestViewController.addMany)
            .append(to: self)
        
        LabelRow(text: "click to go back")
            .click(TestViewController.goBack)
            .append(to: self)
        
        section = DynamicSection(self).finalize(self)
        section.loading = true
        section.emptyRow = true
    }
    
    func addRow() {
        itemCount += 1
        section.batch(inserts: [0], updates: [], deletes: [])
    }
    
    func addMany() {
        itemCount += 3
        section.reload()
    }
    
    func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension TestViewController: DynamicSectionDelegate {
    
    func dynamicSection(skeletonCell section: DynamicSection, repo: CellRepository) -> SkeletonCell {
        return repo.dequeue(for: SimpleSkeletonCell.self)
    }

    func dynamicSection(heightForSkeletonCell section: DynamicSection) -> CGFloat {
        return 50
    }
    
    func dynamicSection(cellCount section: DynamicSection) -> Int {
        return itemCount
    }
    
    func dynamicSection(cell section: DynamicSection, row: Int, repo: CellRepository) -> UICollectionViewCell {
        let cell = repo.dequeue(for: LabelCell.self)
        cell.label.text = String(row)
        return cell
    }
    
    func dynamicSection(selected section: DynamicSection, row: Int, repo: CellRepository) {
        itemCount -= 1
        section.batch(inserts: [], updates: [], deletes: [row])
    }
    
    func dynamicSection(height section: DynamicSection, row: Int) -> CGFloat {
        return 50
    }
    
    func dynamicSection(heightForEmptyCell section: DynamicSection) -> CGFloat {
        return 50
    }
    
    func dynamicSection(emptyCell section: DynamicSection, repo: CellRepository) -> UICollectionViewCell {
        let cell = repo.dequeue(for: LabelCell.self)
        cell.label.text = "empty"
        return cell
    }
    
}
