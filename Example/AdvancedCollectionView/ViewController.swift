//
//  ViewController.swift
//  AdvancedCollectionView
//
//  Created by mohsenShakiba on 08/09/2017.
//  Copyright (c) 2017 mohsenShakiba. All rights reserved.
//

import UIKit
import AdvancedCollectionView

class ViewController: AdvancedCollectionViewwController {
    
    override func setup() {
        
        LabelRow(text: "first test")
            .click(ViewController.goToTestViewController)
            .append(to: self)
        
        LabelRow(text: "second test")
            .append(to: self)
        
    }
    
    func goToTestViewController() {
        let vc = TestViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
}

