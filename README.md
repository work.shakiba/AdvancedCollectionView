# AdvancedCollectionView

[![CI Status](http://img.shields.io/travis/mohsenShakiba/AdvancedCollectionView.svg?style=flat)](https://travis-ci.org/mohsenShakiba/AdvancedCollectionView)
[![Version](https://img.shields.io/cocoapods/v/AdvancedCollectionView.svg?style=flat)](http://cocoapods.org/pods/AdvancedCollectionView)
[![License](https://img.shields.io/cocoapods/l/AdvancedCollectionView.svg?style=flat)](http://cocoapods.org/pods/AdvancedCollectionView)
[![Platform](https://img.shields.io/cocoapods/p/AdvancedCollectionView.svg?style=flat)](http://cocoapods.org/pods/AdvancedCollectionView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdvancedCollectionView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "AdvancedCollectionView"
```

## Author

mohsenShakiba, work.shakiba@gmail.com

## License

AdvancedCollectionView is available under the MIT license. See the LICENSE file for more info.
